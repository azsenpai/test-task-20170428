USING
-----
1. `git clone https://coder_iitu@bitbucket.org/coder_iitu/test-task-20170428.git`

2. `cd test-task-20170428`

3. `composer install`

4. `php -S localhost:8080 -t web`

5. Check the address http://localhost:8080
