<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use app\models\Trip;
use app\models\TripService;
use app\models\FlightSegment;
use app\models\AirportName;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     *
     */
    public function actionList($airportNameValue)
    {
        $airportName = AirportName::findOne(['value' => trim($airportNameValue)]);
        $trips = [];

        if ($airportName) {
            $trips = Trip::find()
                ->joinWith('tripServices.flightSegment', false)
                ->where([
                    'corporate_id' => 3,
                    sprintf('%s.service_id', TripService::tableName()) => 2,
                    sprintf('%s.arrAirportId', FlightSegment::tableName()) => $airportName->airport_id,
                ])
                ->asArray()
                ->all();
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $trips,
            'pagination' => false,
        ]);

        return $this->render('list', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
