<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Test Task';
?>
<div class="site-index">
    <?= Html::beginForm(['list'], 'get') ?>

    <div class="row">
        <div class="col-lg-4">
            <?= Html::textInput('airportNameValue', null, [
                'class' => 'form-control',
                'placeholder' => 'Airport Name',
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?= Html::endForm() ?>
</div>
