<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Trip;

/* @var $this yii\web\View */

$this->title = 'Trip List';
?>
<div class="site-list">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => Trip::attributes(),
    ]); ?>
</div>
